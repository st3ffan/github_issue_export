#!/bin/bash

curl -s -H "Authorization: bearer $(cat key.txt)" -X POST -d @query-issues.json https://api.github.com/graphql | jq -r '.data.repository.issues.edges[].node| [ .title, .url, .state ] | @csv'

